using ETModel;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ETEditor
{
    [InitializeOnLoad]
    public class LocalizationInitializer
    {
        /// <summary>
        /// ´´˝¨LocalizationService
        /// </summary>
        [MenuItem("Tools/Foundation/Initialize LocalizationService")]
        public static void Startup()
        {
            {
                var inst = Resources.Load<LocalizationService>("LocalizationService");
                if (inst == null)
                {
                    var path = Application.dataPath + "/Resources";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    var asset = ScriptableObject.CreateInstance(typeof (LocalizationService));
                    Debug.Log(asset);
                    AssetDatabase.CreateAsset(asset, "Assets/Resources/LocalizationService.asset");
                    AssetDatabase.SaveAssets();

                    Debug.Log("LocalizationService Created");
                }

            }

        }
    }
}
