using ETModel;
using UnityEditor;

namespace ETEditor
{
    /// <summary>
    /// 导入Localization时，重新初始化读取文本
    /// </summary>
    public class LocalizationPostprocessor : AssetPostprocessor
    {
        public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            LocalizationInitializer.Startup();

            foreach (string asset in importedAssets)
            {
                if (asset.Contains("Localization"))
                {
                    var service = LocalizationService.Instance;
                    if (service != null)
                        service.LoadTextAssets();

                    return;
                }
            }

        }
    }
}