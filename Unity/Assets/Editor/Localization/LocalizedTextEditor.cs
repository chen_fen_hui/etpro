using ETModel;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ETEditor
{
    /// <summary>
    /// LocalizedText���
    /// </summary>
    [CustomEditor(typeof(LocalizedText), true)]
    public class LocalizedTextEditor : UnityEditor.Editor
    {
        protected LocalizedText Target;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

         //   LocalizationInitializer.Startup();

            Target = target as LocalizedText;

            if (Application.isPlaying)
                return;

            var service = LocalizationService.Instance;

            if (service != null)
            {
                var p = EditorGUILayout.LongField("Key", Target.Key);

                if (p != Target.Key)
                {
                    Target.Key = p;
                    EditorUtility.SetDirty(target);
                }
                EditorGUILayout.LabelField("Value", service.Get(p));
            }
           
        }
    }
}


