﻿using System;
using ETModel;
using UnityEngine;

namespace ETHotfix
{
    public static class UILobbyFactory
    {
        public static void Create()
        {
	        try
	        {
				ResourcesComponent resourcesComponent = ETModel.Game.Scene.GetComponent<ResourcesComponent>();
		        resourcesComponent.LoadBundle(UIType.UILobby.StringToAB());
				GameObject bundleGameObject = (GameObject)resourcesComponent.GetAsset(UIType.UILobby.StringToAB(), UIType.UILobby);
				GameObject gameObject = UnityEngine.Object.Instantiate(bundleGameObject);
		        UI ui = ComponentFactory.Create<UI, string, GameObject>(UIType.UILobby, gameObject, false);

				ui.AddComponent<UILobbyComponent>();

				Game.Scene.GetComponent<UIComponent>().Add(ui, UILayers.NormalLayer);

			}
	        catch (Exception e)
	        {
				Log.Error(e);
		        return;
	        }
		}

		public static void Remove()
		{
			Game.Scene.GetComponent<UIComponent>().Remove(UIType.UILobby,UILayers.NormalLayer);
			ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle(UIType.UILobby.StringToAB());
		}
    }
}