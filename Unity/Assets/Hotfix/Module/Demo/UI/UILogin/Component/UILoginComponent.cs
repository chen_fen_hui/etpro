﻿using System;
using System.Net;
using ETModel;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace ETHotfix
{
	[ObjectSystem]
	public class UiLoginComponentSystem : AwakeSystem<UILoginComponent>
	{
		public override void Awake(UILoginComponent self)
		{
			//在此填充Awake的逻辑
			//可以再调用UILoginComponent的Awake方法
			self.Awake();
		}
	}

	[ObjectSystem]
	public class UiLoginComponentStartSystem : StartSystem<UILoginComponent>
	{
		public override void Start(UILoginComponent self)
		{
			Log.Debug("在此实现Start的逻辑!");
		}
	}


	public class UILoginComponent: Component
	{
		private GameObject account;
		private GameObject loginBtn;
		private GameObject saveBtn;
		private GameObject newBtn;
		private GameObject ScoreBtn;
		private GameObject continueBtn;
		private Text scoreText;
		private Text staticText;
		private Text animText;
		private GameObject lanBtn;

		public void Awake()
		{
			ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
			loginBtn = rc.Get<GameObject>("LoginBtn");
			loginBtn.GetComponent<Button>().onClick.Add(OnLogin);
			this.account = rc.Get<GameObject>("Account");
			saveBtn = rc.Get<GameObject>("saveBtn");
			saveBtn.GetComponent<Button>().onClick.Add(OnSave);
			newBtn = rc.Get<GameObject>("newBtn");
			newBtn.GetComponent<Button>().onClick.Add(OnNew);
			ScoreBtn = rc.Get<GameObject>("ScoreBtn");
			ScoreBtn.GetComponent<Button>().onClick.Add(OnScore);
			continueBtn = rc.Get<GameObject>("continueBtn");
            continueBtn.GetComponent<Button>().onClick.Add(OnContinie);
            scoreText = rc.Get<GameObject>("scoreText").GetComponent<Text>();
			//scoreText.transform.DOScale(Vector3.zero, 3f).SetLoops(-1,LoopType.Yoyo).SetEase(Ease.Linear);
			lanBtn = rc.Get<GameObject>("lanBtn");
			lanBtn.GetComponent<Button>().onClick.Add(OnLanBtn);
			staticText = rc.Get<GameObject>("staticText").GetComponent<Text>();
			animText = rc.Get<GameObject>("animText").GetComponent<Text>();
			RefreshAnimText();
			ETModel.Game.Scene.GetComponent<LocaliztionComponent>().OnLanguageChanged(RefreshAnimText);
		}

        private void RefreshAnimText()
        {
			animText.text = string.Format(ETModel.Game.Scene.GetComponent<LocaliztionComponent>().GetTextByKey(1002), 3);
		}

        public void OnLanBtn()
        {
			LanguageInfo info = LanguageInfo.All[UnityEngine.Random.Range(0, LanguageInfo.All.Length)];
			ETModel.Game.Scene.GetComponent<LocaliztionComponent>().ChangeLanguage(info);
		}

        public void OnSave()
		{
			Game.Scene.GetComponent<SaveComponent>().JSONSave();
		}
		public void OnNew()
		{
			Game.Scene.GetComponent<SaveComponent>().NewGame();
			scoreText.text = Game.Scene.GetComponent<SaveComponent>().score.ToString();
			//	Game.Scene.GetComponent<SaveComponent>().ContinueGame();
		}
		public void OnContinie()
		{
			//Game.Scene.GetComponent<SaveComponent>().NewGame();
				Game.Scene.GetComponent<SaveComponent>().ContinueGame();
			scoreText.text = Game.Scene.GetComponent<SaveComponent>().score.ToString();
		}
		public void OnScore()
		{
			
			Game.Scene.GetComponent<SaveComponent>().score++;
			scoreText.text = Game.Scene.GetComponent<SaveComponent>().score.ToString();
		}
		public void OnLogin()
		{
			LoginHelper.OnLoginAsync(this.account.GetComponent<InputField>().text).Coroutine();
		}
	}
}
