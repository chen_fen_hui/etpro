﻿using ETModel;

namespace ETHotfix
{
	[Event(EventIdType.LoginFinish)]
	public class LoginFinish_RemoveLoginUI: AEvent
	{
		public override void Run()
		{
			UILoginFactory.Remove();
		}
	}
}
