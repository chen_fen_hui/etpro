﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ETHotfix
{

    [System.Serializable]
    public class SaveInfo
    {
        public int score = 0;
    }
}