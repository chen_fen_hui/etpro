﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ETModel;

namespace ETHotfix
{

    [ObjectSystem]
    public class UIEffectComponentAwakeSystem : AwakeSystem<UIEffectComponent>
    {
        public override void Awake(UIEffectComponent self)
        {
            self.Awake();
        }
    }
    public class UIEffectComponent : Component
    {
        private const string effectAbName = "effect";

        private Dictionary<string, UIEffect> uiEffectDic = new Dictionary<string, UIEffect>();

        public void Awake()
        {
            ResourcesComponent resourcesComponent = ETModel.Game.Scene.GetComponent<ResourcesComponent>();
            resourcesComponent.LoadBundle(effectAbName.StringToAB());
        }
        public UIEffect Add(Transform parent, int sortingOrder, UIEffectClass effectConfig, Action createCallback = null)
        {           
            UIEffect effect = ComponentFactory.Create<UIEffect, int, UIEffectClass, Action>(sortingOrder, effectConfig, createCallback, true);
            effect.GameObject.transform.SetParent(parent);
            this.uiEffectDic.Add(effect.Name, effect);
            return effect;
        }

        public void Remove(UIEffect effect)
        {
            UIEffect ui;
            if (!this.uiEffectDic.TryGetValue(effect.Name, out ui))
            {
                return;
            }
            this.uiEffectDic.Remove(effect.Name);
            ui.Dispose();
        }

    }
}

