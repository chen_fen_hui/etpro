﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ETHotfix
{
    public class UIEffectClass
    {
        public string name;     //名字
        public string effectPath;  //特效路劲
        public bool isLoop;    //是否循环
        public long liveTime; //存活时间(毫米),-1永久
    }
    public class UIEffectConfig
    {
        public static UIEffectClass UITestEffect = new UIEffectClass()
        {
            name = "UITestEffect",
            effectPath = "todo",
            isLoop = false,
            liveTime = 3000,
        };
    }
}
