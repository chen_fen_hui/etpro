using System;
using UnityEngine;

namespace ETModel
{
    
    [Serializable]
    public class LanguageInfo : IEquatable<LanguageInfo>
    {
        public string Name;//����
        public SystemLanguage lan;//ϵͳ����

        public LanguageInfo()
        {

        }

        public LanguageInfo(string name, SystemLanguage sl)
        {
            Name = name;
            lan = sl;
        }

        /// <summary>
        /// ������������
        /// </summary>
        public static readonly LanguageInfo Chinese = new LanguageInfo("Chinese", SystemLanguage.Chinese);
        public static readonly LanguageInfo English = new LanguageInfo("English", SystemLanguage.English);
        

        public static readonly LanguageInfo[] All = {
            Chinese,
            English,                      
        };

        public bool Equals(LanguageInfo other)
        {
            return other.Name == Name;
        }
    }
}