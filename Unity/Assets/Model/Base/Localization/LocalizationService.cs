using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace ETModel
{
   
    public class LocalizationService : ScriptableObject
    {
        #region static
        private static LocalizationService _instance;
        public static LocalizationService Instance
        {
            get { return _instance ?? (_instance = Create()); }
        }

        /// <summary>
        /// LocalizationService的asset文件
        /// </summary>
        /// <returns></returns>
        static LocalizationService Create()
        {
            return Resources.Load<LocalizationService>("LocalizationService");
        }

        #endregion

        #region event
        /// <summary>
        /// 语言改变事件
        /// </summary>
        public static Action OnLanguageChanged;
        #endregion

        #region properties

        /// <summary>
        /// 存储所有语言信息
        /// </summary>
        public Dictionary<long, string> Strings = new Dictionary<long, string>();      

        /// <summary>
        /// 默认显示静态文本语言
        /// </summary>
        [HideInInspector]
        public LanguageInfo DefaultLanguage;

        /// <summary>
        /// 当前语言
        /// </summary>
        [HideInInspector]
        private LanguageInfo _language = LanguageInfo.Chinese;
        public LanguageInfo Language
        {
            get { return _language; }
            set
            {
                if (!HasLanguage(value))
                {
                    Debug.LogError("Invalid Language " + value);
                }

                _language = value;

                RaiseLanguageChanged();

                SaveToPrefs();
            }
        }

        /// <summary>
        /// 所有支持的语言
        /// </summary>
        [SerializeField]
        public LanguageInfo[] Languages = LanguageInfo.All;
        #endregion

        #region LocalizationService       

        public void Init()
        {
            LoadLanguage();

#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }
#endif
         //   ReadFiles();
        }

        /// <summary>
        /// 默认加载语言
        /// </summary>
        void LoadLanguage()
        {
            var raw = PlayerPrefs.GetString("CurrentLanguage");
            if (string.IsNullOrEmpty(raw))
            {
                bool hasLan = false;
                for (int i = 0; i < Languages.Length; i++)
                {
                    if (Application.systemLanguage == Languages[i].lan)
                    {
                        hasLan = true;
                        Language = Languages[i];
                        break;
                    }
                }
                if (!hasLan)
                {
                    Language = LanguageInfo.Chinese;
                }
                return;
            }


            var lan = Languages.FirstOrDefault(o => o.Name == raw);
            if (lan == null)
            {
                Debug.LogError("Unknown language saved to prefs : " + raw);
                Language = LanguageInfo.Chinese;
            }
            else
            {
                Language = lan;
            }
        }

        /// <summary>
        /// 保存到本地
        /// </summary>
        void SaveToPrefs()
        {
            PlayerPrefs.SetString("CurrentLanguage", Language.Name);
            PlayerPrefs.Save();
        }

        #endregion

        #region internal

        /// <summary>
        /// 是否有配置该语言
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        bool HasLanguage(LanguageInfo language)
        {
            foreach (var systemLanguage in Languages)
            {
                if (systemLanguage.Equals(language))
                    return true;
            }
            return false;
        }


        /// <summary>
        /// 切换语言
        /// </summary>
        private void RaiseLanguageChanged()
        {
            
            if (Application.isPlaying)
            {
                ReadFiles();
                if (OnLanguageChanged != null)
                    OnLanguageChanged();
            }
        }

        /// <summary>
        /// 读取当前语言配置（ET为例）
        /// </summary>
        void ReadFiles()
        {
            Strings.Clear();

            var configs = Game.Scene.GetComponent<ConfigComponent>().GetAll(typeof(Localization));

            if (!configs.Any())
            {
                Debug.LogError("Localization Files Not Found : " + Language.Name);
            }


            foreach (Localization config in configs)
            {
                ReadCSVAsset(config);
            }
        }

        /// <summary>
        /// 读取单条配置到缓存
        /// </summary>
        /// <param name="config"></param>
        void ReadCSVAsset(Localization config)
        {
            try
            {
                FieldInfo info = typeof(Localization).GetField(Language.Name);
                Strings.Add(config.Id, info.GetValue(config).ToString());

            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
                Debug.LogError(string.Format("Failed to read file : {0}/{1}  ", Language, config.Id));
            }

        }
        #endregion

        #region public

        /// <summary>
        /// 非运行模式下加载文本TODO
        /// </summary>
        public void LoadTextAssets()
        {
            // ReadFiles();
#if UNITY_EDITOR
            Strings.Clear();
            var jsonObj = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Res/Config/Localization.txt", typeof(TextAsset)) as TextAsset;
            string[] jsonArray = jsonObj.text.Split('\n');
            foreach (string tx in jsonArray)
            {
                if (string.IsNullOrEmpty(tx))
                    continue;
                Localization json = JsonHelper.FromJson<Localization>(tx);
                FieldInfo info = typeof(Localization).GetField(DefaultLanguage.Name);
                Strings.Add(json.Id, info.GetValue(json).ToString());
            }           
#endif
        }

        /// <summary>
        /// 从字典获取多语言文本
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string Get(long key)
        {
            return Get(key, string.Empty);
        }

        /// <summary>
        /// 获取多语言文本，没有则返回默认值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="fallback"> if not found</param>
        /// <returns></returns>
        public string Get(long key, string fallback)
        {
            if (!Strings.ContainsKey(key))
            {
             //   Debug.LogWarning(string.Format("Localization Key Not Found {0} : {1} ", Language.Name, key));
                return fallback;
            }

            return Strings[key];
        }


        #endregion


    }
}