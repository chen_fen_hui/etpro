using UnityEngine;
using UnityEngine.UI;

namespace ETModel
{
    /// <summary>
    /// ��̬�ı�
    /// </summary>
    [RequireComponent(typeof(Text))]
    [AddComponentMenu("Localization/LocalizedText")]
    public class LocalizedText : MonoBehaviour
    {

        [HideInInspector]
        public long Key;

        private void Awake()
        {
            OnLocalization();
            LocalizationService.OnLanguageChanged += OnLocalization;
        }

        private void OnDestroy()
        {
            LocalizationService.OnLanguageChanged -= OnLocalization;
        }

        public void OnLocalization()
        {
            var label = GetComponent<Text>();

            label.text = Game.Scene.GetComponent<LocaliztionComponent>().GetTextByKey(Key);
        }

    }
}