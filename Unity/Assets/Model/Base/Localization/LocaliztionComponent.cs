﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ETModel
{
    [ObjectSystem]
    public class LocaliztionAwakeSystem : AwakeSystem<LocaliztionComponent>
    {
        public override void Awake(LocaliztionComponent self)
        {
            LocalizationService.Instance.Init();
        }
    }

    [ObjectSystem]
    public class LocaliztionDestroySystem : DestroySystem<LocaliztionComponent>
    {
        public override void Destroy(LocaliztionComponent self)
        {
            if (LocalizationService.Instance.Language != LocalizationService.Instance.DefaultLanguage)
                LocalizationService.Instance.LoadTextAssets();
        }
    }
    public class LocaliztionComponent : Component
    {
        /// <summary>
        /// 获取文本
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetTextByKey(long key)
        {
            return LocalizationService.Instance.Get(key);
        }

        /// <summary>
        /// 切换语言
        /// </summary>
        /// <param name="info"></param>
        public void ChangeLanguage(LanguageInfo info)
        {
            LocalizationService.Instance.Language = info;
        }

        /// <summary>
        /// 动态文本如果切换语言时没有手动刷新界面的话，需要加上这个回调强制刷新
        /// </summary>
        /// <param name="act"></param>
        public void OnLanguageChanged(Action act)
        {
            LocalizationService.OnLanguageChanged += act;
        }
    }
}

