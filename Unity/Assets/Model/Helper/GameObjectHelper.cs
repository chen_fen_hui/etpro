﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace ETModel
{
	public static class GameObjectHelper
	{
		public static T Get<T>(this GameObject gameObject, string key) where T : class
		{
			try
			{
				return gameObject.GetComponent<ReferenceCollector>().Get<T>(key);
			}
			catch (Exception e)
			{
				throw new Exception($"获取{gameObject.name}的ReferenceCollector key失败, key: {key}", e);
			}
		}

		public static List<T> GetAll<T>(this GameObject gameObject) where T : class
		{
			try
			{
				return gameObject.GetComponent<ReferenceCollector>().GetAll<T>();
			}
			catch (Exception e)
			{
				throw new Exception($"获取{gameObject.name}的ReferenceCollector 所有节点失败", e);
			}
		}
	}
}