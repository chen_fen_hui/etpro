using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class UILayer_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(global::UILayer);

            field = type.GetField("name", flag);
            app.RegisterCLRFieldGetter(field, get_name_0);
            app.RegisterCLRFieldSetter(field, set_name_0);
            field = type.GetField("orderInLayer", flag);
            app.RegisterCLRFieldGetter(field, get_orderInLayer_1);
            app.RegisterCLRFieldSetter(field, set_orderInLayer_1);
            field = type.GetField("planeDistance", flag);
            app.RegisterCLRFieldGetter(field, get_planeDistance_2);
            app.RegisterCLRFieldSetter(field, set_planeDistance_2);


        }



        static object get_name_0(ref object o)
        {
            return ((global::UILayer)o).name;
        }
        static void set_name_0(ref object o, object v)
        {
            ((global::UILayer)o).name = (System.String)v;
        }
        static object get_orderInLayer_1(ref object o)
        {
            return ((global::UILayer)o).orderInLayer;
        }
        static void set_orderInLayer_1(ref object o, object v)
        {
            ((global::UILayer)o).orderInLayer = (System.Int32)v;
        }
        static object get_planeDistance_2(ref object o)
        {
            return ((global::UILayer)o).planeDistance;
        }
        static void set_planeDistance_2(ref object o, object v)
        {
            ((global::UILayer)o).planeDistance = (System.Int32)v;
        }


    }
}
