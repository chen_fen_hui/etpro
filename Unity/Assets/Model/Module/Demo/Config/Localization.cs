namespace ETModel
{
	[Config((int)(AppType.ClientH |  AppType.ClientM | AppType.Gate | AppType.Map))]
	public partial class LocalizationCategory : ACategory<Localization>
	{
	}

	public class Localization: IConfig
	{
		public long Id { get; set; }
		public string Chinese;
		public string English;
		public string Spanish;
	}
}
