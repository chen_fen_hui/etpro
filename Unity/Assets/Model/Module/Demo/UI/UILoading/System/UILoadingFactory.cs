﻿using System;
using UnityEngine;

namespace ETModel
{
    public static class UILoadingFactory
    {
        public static void Create()
        {
	        try
	        {
				GameObject bundleGameObject = ((GameObject)ResourcesHelper.Load("KV")).Get<GameObject>(UIType.UILoading);
				GameObject go = UnityEngine.Object.Instantiate(bundleGameObject);
				go.layer = LayerMask.NameToLayer(LayerNames.UI);
				UI ui = ComponentFactory.Create<UI, string, GameObject>(UIType.UILoading, go, false);

				ui.AddComponent<UILoadingComponent>();
				Game.Scene.GetComponent<UIComponent>().Add(ui,UILayers.TopLayer);
			}
	        catch (Exception e)
	        {
				Log.Error(e);
		        return;
	        }
		}

	    public static void Remove()
	    {
			Game.Scene.GetComponent<UIComponent>().Remove(UIType.UILoading,UILayers.TopLayer);
		}
    }
}