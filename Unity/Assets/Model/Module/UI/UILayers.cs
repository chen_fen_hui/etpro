﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UILayer
{
    public string name;
    public int planeDistance;
    public int orderInLayer;
};

public class UILayers
{
    //场景UI，如：点击建筑查看建筑信息---一般置于场景之上，界面UI之下
    public static UILayer SceneLayer = new UILayer(){ name = "SceneLayer", planeDistance = 1000,orderInLayer = 0 };
    //背景UI，如：主界面---一般情况下用户不能主动关闭，永远处于其它UI的最底层
    public static UILayer BackgroudLayer = new UILayer() { name = "BackgroudLayer", planeDistance = 900, orderInLayer = 1000 };
    //普通UI，一级、二级、三级等窗口---一般由用户点击打开的多级窗口
    public static UILayer NormalLayer = new UILayer() { name = "NormalLayer", planeDistance = 800, orderInLayer = 2000 };
    //信息UI---如：跑马灯、广播等---一般永远置于用户打开窗口顶层
    public static UILayer InfoLayer = new UILayer() { name = "InfoLayer", planeDistance = 700, orderInLayer = 3000 };
    //提示UI，如：错误弹窗，网络连接弹窗等
    public static UILayer TipLayer = new UILayer() { name = "TipLayer", planeDistance = 600, orderInLayer = 4000 };
    //顶层UI，如：场景加载
    public static UILayer TopLayer = new UILayer() { name = "TopLayer", planeDistance = 500, orderInLayer = 5000 };
}
